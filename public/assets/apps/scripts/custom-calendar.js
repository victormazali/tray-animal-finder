
document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
      plugins: [ 'interaction', 'timeGrid' ],
      allDaySlot: false,
      slotEventOverlap: false,
      timeZone: 'UTC',
      editable: true,
      droppable: true, // this allows things to be dropped onto the calendar !!!
      // limita horario e dias que ficaram visíveis 
      header: {
        left: 'title',
        center: '',
        right: 'today, timeGridMonth, timeGridWeek, timeGridDay, prev, next'
      },
      minTime: '08:00',
      maxTime: '18:00',
      hiddenDays: [ 0 ],
      locale: 'pt-br',
      // events : "{{ route('schedule.getEvents') }}",
      // events: [{
      //     title: 'Ocupado',
      //     start: '2019-08-26 08:00',
      //     end: '2019-08-26 09:00',
      //     editable: false, 
      //     backgroundColor: App.getBrandColor('red')
      //   },  {
      //       title: 'Cliente A',
      //       start: '2019-08-26 09:00',
      //       end: '2019-08-26 10:00',
      //       backgroundColor: App.getBrandColor('green')
      //   },  {
      //       title: 'Cliente B',
      //       start: '2019-08-26 14:00',
      //       end: '2019-08-26 15:00',
      //       backgroundColor: App.getBrandColor('green')
      //   }, {
      //       title: 'Almoço',
      //       start: '2019-08-26 11:00',
      //       end: '2019-08-26 13:00',
      //       editable: false, 
      //       backgroundColor: App.getBrandColor('grey')
      //   }
      // ]
    });

  calendar.render();
});