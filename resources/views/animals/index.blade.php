@extends('layouts.default')

@section('title')
  Meus Animais
@endsection

@section('breadcumb')
	<li>
		Animais
	</li>
@endsection

@section('content')
	
    <h1 class="page-title">
    	Animais
        <small>Index</small>
    </h1>

	<a class="btn blue-sharp btn-outline sbold pull-right" href="{{ route('animals.create') }}"><i class="fa fa-paw"></i> Cadastrar Animal</a>
	<div style="margin:0; padding:0; height:50px;">&nbsp;</div>

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">Meus Animais</b></span>
                    </div>
                    <div class="tools">

                    </div>
                </div>
                <div class="portlet-body table-both-scroll">
                    <table class="table table-striped table-bordered table-hover order-column" id="">
                        <thead>
                            <tr>
                                <th>Foto</th>
                                <th>Nome</th>
                                <th>Idade</th>
                                <th>Informações</th>
                                <th>Cidade</th>
                                <th>Estado</th>
                                <th>Status</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($result as $r)
                            <tr>
                                <td>
                                    <img class="img-responsive" style="width: 100px; height: 100px;" src="{{asset($r->picture)}}">
                                </td>
                                <td>{{$r->name}}</td>
                                <td>{{$r->age}}</td>
                                <td>{{$r->note}}</td>
                                <td>{{$r->city}}</td>
                                <td>{{$r->state}}</td>
                                <td>{{$r->status->description}}</td>
                                <td>
                                    <div class="btn-group">
                                        <button class="btn btn-xs red dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Ações
                                            <i class="fa fa-angle-down"></i>
                                        </button>
                                        <ul class="dropdown-menu pull-left" role="menu">
                                            @if ($r->status_id === 2)
                                            <li>
                                                <a href="{{ route('animals.showFinders', $r->id) }}">
                                                    <i class="fa fa-edit"></i> Ver comunicados </a>
                                            </li>
                                            @endif
                                            <li>
                                                <a href="{{ route('animals.edit', $r->id) }}">
                                                    <i class="fa fa-edit"></i> Editar </a>
                                            </li>
                                            <li>
                                            <form role="form" action="{{ route('animals.destroy', $r->id) }}" METHOD="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}<button type="submit">Remover</button>
                                            </form>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach    
                        </tbody>
                    </table>
                    {{ $result->appends($_GET)->links() }}
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
    
@endsection

@section('before-plugins')
        <!--Datatable-->
        <script src="{{ asset("assets/global/scripts/datatable.js") }}" type="text/javascript"></script>
        <script src="{{ asset("assets/global/plugins/datatables/datatables.min.js") }}" type="text/javascript"></script>
        <script src="{{ asset("assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js") }}" type="text/javascript"></script>
        <script src="{{ asset("assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js") }}" type="text/javascript"></script>

@endsection

@section('after-plugins')
        <!--Datatable-->
        <script src="{{ asset("assets/pages/scripts/table-datatables-buttons.min.js") }}" type="text/javascript"></script>


@endsection

@section('scripts')
 
@endsection