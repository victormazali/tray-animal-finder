<div class="form-body">
    <div class="alert alert-danger display-hide">
    <button class="close" data-close="alert"></button>
    Você tem alguns erros de formulário. Por favor, verifique abaixo.
    </div>
    <div class="alert alert-success display-hide">
    <button class="close" data-close="alert"></button> Sua validação de formulário foi bem sucedida! </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Nome
            <span class="required"> * </span></label>
            <div class="col-md-5">
                <div class="input-icon right">
                    <i class="fa fa-paw"></i>
                    <input name="name" type="text" class="form-control" placeholder="Nome do Animal" value="{{ old('name') ? old('name') : $animal->name }}"> </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Idade
            <span class="required"> * </span></label>
            <div class="col-md-5">
                <div class="input-icon right">
                    <i class="fa fa-paw"></i>
                    <input name="age" type="text" class="form-control" placeholder="Idade do animal" value="{{ old('age') ? old('age') : $animal->age }}"> </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Informações Extras
            <span class="required"> * </span></label>
            <div class="col-md-5">
                <textarea id="modal-note" name="note" class="form-control" rows="3">{{ old('city') ? old('city') : $animal->note }}
                </textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Cidade
            <span class="required"> * </span></label>
            <div class="col-md-5">
                <div class="input-icon right">
                    <i class="fa fa-home"></i>
                    <input name="city" type="text" class="form-control" placeholder="Cidade do desaparecimento" value="{{ old('city') ? old('city') : $animal->city }}"> </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Estado
            <span class="required"> * </span></label>
            <div class="col-md-5">
                <div class="input-icon right">
                    <i class="fa fa-home"></i>
                    <input name="state" type="text" class="form-control" placeholder="EStado do desaparecimento" value="{{ old('state') ? old('state') : $animal->state }}"> </div>
            </div>
        </div>
        <div class="form-group ">
        <label class="control-label col-md-3">Foto</label>
        <div class="col-md-9">
            <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 100%; height: 100px;"> </div>
                    <div>
                        <span class="btn red btn-outline btn-file">
                          <span class="fileinput-new"> Selecione a Imagen </span>
                          <span class="fileinput-exists"> Trocar </span>
                          <input type="file" name="attachmentTemp"> </span>
                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remover </a>
                    </div>
                </div>
            </div>
        </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-9">
            <button type="submit" class="btn btn-success">Salvar</button>
            <a class="btn default" href="{{ route('animals.index') }}">Cancelar</a>
        </div>
    </div>
</div>