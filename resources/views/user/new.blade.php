@extends('layouts.default')

@section('css-plugins')
        {{-- Select --}}
        <link href="{{ asset("assets/global/plugins/select2/css/select2.min.css") }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset("assets/global/plugins/select2/css/select2-bootstrap.min.css") }}" rel="stylesheet" type="text/css" />

        {{-- File Input --}}
        <link href="{{ asset("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css") }}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcumb')
	<li>
        <a href="{{ action('UserController@index') }}" class="nav-link nav-toggle">
            <span class="title">Usuários</span>
        </a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        Criar
    </li>
@endsection

@section('content')
	
    <h1 class="page-title">
    	Usuários
        <small>Criar</small>
    </h1>

        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-equalizer bold"></i>
                    <span class="caption-subject bold uppercase">Criar Usuário</span>
                    <span class="caption-helper">formulário para criação de usuário</span>
                </div>
                <div class="actions">
                    
                </div>
            </div>
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <form action="{{ route('users.store') }}" METHOD="POST" id="formUser" class="form-horizontal" enctype="multipart/form-data">
                {{ csrf_field() }}
                    @include('user._form')
                </form>
                <!-- END FORM-->
            </div>
        </div>

    
@endsection

@section('before-plugins')
        {{-- Select --}}
        <script src="{{ asset("assets/global/plugins/select2/js/select2.full.min.js") }}" type="text/javascript"></script>

        {{-- Input File --}}
        <script src="{{ asset("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js") }}" type="text/javascript"></script>

        {{-- Mask --}}
        <script src="{{ asset("assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js") }}" type="text/javascript"></script>
        <script src="{{ asset("assets/global/plugins/jquery.input-ip-address-control-1.0.min.js") }}" type="text/javascript"></script>

        {{-- Validation --}}
        <script src="{{ asset("assets/global/plugins/jquery-validation/js/jquery.validate.js") }}" type="text/javascript"></script>
        <script src="{{ asset("assets/global/plugins/jquery-validation/js/additional-methods.js") }}" type="text/javascript"></script>
@endsection

@section('after-plugins')
        {{-- Select --}}
        <script src="{{ asset("assets/pages/scripts/components-select2.min.js") }}" type="text/javascript"></script>

        {{-- Mask --}}
        <script src="{{ asset("assets/pages/scripts/form-input-mask.min.js") }}" type="text/javascript"></script>

        {{-- Validation --}}
        <script src="{{ asset("assets/pages/scripts/form-validation.js") }}" type="text/javascript"></script>
@endsection

@section('script')

@endsection