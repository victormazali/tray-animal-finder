@extends('layouts.default')

@section('title')
    Usuários    
@endsection

@section('css-plugins')
        <!--Datatable-->
        <link href="{{ asset("assets/global/plugins/datatables/datatables.min.css") }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset("assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css") }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset("assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css") }}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcumb')
	<li>
            Usuários
	</li>
@endsection

@section('content')
	
    <h1 class="page-title">
    	Usuários
        <small>Index</small>
    </h1>

	<a class="btn blue-sharp btn-outline sbold pull-right" href="{{ route('users.create') }}"><i class="fa fa-user-plus"></i> Novo Usuário</a>
	<div style="margin:0; padding:0; height:50px;">&nbsp;</div>

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">Usuários</b></span>
                    </div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="sample_1">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Email</th>
                                <th>CPF</th>
                                <th>Departamento</th>
                                <th>Cargo</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                    <tbody>
                        @foreach($result as $r)
                            <tr>
                                <td>{{$r->name}}</td>
                                <td>{{$r->email}}</td>
                                <td>{{$r->cpf}}</td>
                                <td>{{$r->department->description}}</td>
                                <td>{{isset($r->roles->first()->name) ? $r->roles->first()->name : "" }}</td>
                                <td>
                                    <div class="btn-group">
                                        <button class="btn btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Ações
                                            <i class="fa fa-angle-down"></i>
                                        </button>
                                        <ul class="dropdown-menu pull-left" role="menu">
                                            <li>
                                                <a href="{{ route('users.edit', $r->id) }}">
                                                    <i class="fa fa-edit"></i> Editar </a>
                                            </li>
                                            <li>
                                            <form role="form" action="{{ action('UserController@destroy', $r->id) }}" METHOD="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}<button type="submit">Desativar</button>
                                            </form>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
    
@endsection

@section('before-plugins')
        <!--Datatable-->
        <script src="{{ asset("assets/global/scripts/datatable.js") }}" type="text/javascript"></script>
        <script src="{{ asset("assets/global/plugins/datatables/datatables.min.js") }}" type="text/javascript"></script>
        <script src="{{ asset("assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js") }}" type="text/javascript"></script>
        <script src="{{ asset("assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js") }}" type="text/javascript"></script>
@endsection

@section('after-plugins')
        <!--Datatable-->
        <script src="{{ asset("assets/pages/scripts/table-datatables-buttons.min.js") }}" type="text/javascript"></script>

@endsection

@section('scripts')
 
@endsection