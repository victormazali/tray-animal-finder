@extends('layouts.default')

@section('css-plugins')
        {{-- Select --}}
        <link href="{{ asset("assets/global/plugins/select2/css/select2.min.css") }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset("assets/global/plugins/select2/css/select2-bootstrap.min.css") }}" rel="stylesheet" type="text/css" />

        {{-- File Input --}}
        <link href="{{ asset("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css") }}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcumb')
	<li>
        <a href="#" class="nav-link nav-toggle">
            <span class="title">Usuários</span>
        </a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        Alterar Senha
    </li>
@endsection

@section('content')
	
    <h1 class="page-title">
    	Usuários
        <small>Editar</small>
    </h1>

        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-equalizer bold"></i>
                    <span class="caption-subject bold uppercase">Alterar senha</span>
                    <span class="caption-helper">formulário para edição do usuário</span>
                </div>
                <div class="actions">
                    
                </div>
            </div>
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <form action="{{ route('users.update', Auth::user()->id) }}" METHOD="POST" id="formUser" class="form-horizontal" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="form-body">
                    <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    Você tem alguns erros de formulário. Por favor, verifique abaixo.
                    </div>
                    <div class="alert alert-success display-hide">
                    <button class="close" data-close="alert"></button> Sua validação de formulário foi bem sucedida! </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Nova Senha
                            <span class="required"> * </span></label>
                            <div class="col-md-5" data-toggle="popover" data-placement="top" data-content="Deve ter no mínimo 6 caracteres.">
                                <div class="input-icon right">
                                    <i class="fa fa-lock"></i>
                                    <input name="password" id="password" type="password" class="form-control" placeholder="Password"> 
                                    <span class="help-inline"> Deve ter no mínimo 6 caracteres. </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Confirmar Senha
                            <span class="required"> * </span></label>
                            <div class="col-md-5" data-toggle="popover" data-placement="top" data-content="Deve ter no mínimo 6 caracteres.">
                                <div class="input-icon right">
                                    <i class="fa fa-lock"></i>
                                    <input name="password_confirm" name="password_confirm" type="password" class="form-control" placeholder="Password">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn btn-success">Salvar</button>
                                <a class="btn default" href="{{ route('home') }}">Cancelar</a>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>

    
@endsection

@section('before-plugins')
        {{-- Select --}}
        <script src="{{ asset("assets/global/plugins/select2/js/select2.full.min.js") }}" type="text/javascript"></script>

        {{-- Input File --}}
        <script src="{{ asset("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js") }}" type="text/javascript"></script>

        {{-- Mask --}}
        <script src="{{ asset("assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js") }}" type="text/javascript"></script>
        <script src="{{ asset("assets/global/plugins/jquery.input-ip-address-control-1.0.min.js") }}" type="text/javascript"></script>

        {{-- Validation --}}
        <script src="{{ asset("assets/global/plugins/jquery-validation/js/jquery.validate.js") }}" type="text/javascript"></script>
        <script src="{{ asset("assets/global/plugins/jquery-validation/js/additional-methods.js") }}" type="text/javascript"></script>
@endsection

@section('after-plugins')
        {{-- Select --}}
        <script src="{{ asset("assets/pages/scripts/components-select2.min.js") }}" type="text/javascript"></script>

        {{-- Mask --}}
        <script src="{{ asset("assets/pages/scripts/form-input-mask.min.js") }}" type="text/javascript"></script>

        {{-- Validation --}}
        <script src="{{ asset("assets/pages/scripts/form-validation.js") }}" type="text/javascript"></script>
@endsection

@section('script')

@endsection