<div class="form-body">
    <div class="alert alert-danger display-hide">
    <button class="close" data-close="alert"></button>
    Você tem alguns erros de formulário. Por favor, verifique abaixo.
    </div>
    <div class="alert alert-success display-hide">
    <button class="close" data-close="alert"></button> Sua validação de formulário foi bem sucedida! </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Nome
            <span class="required"> * </span></label>
            <div class="col-md-5">
                <div class="input-icon right">
                    <i class="fa fa-user"></i>
                    <input name="name" type="text" class="form-control" placeholder="Enter name" value="{{ old('name') ? old('name') : $user->name }}"> </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Email
            <span class="required"> * </span></label>
            <div class="col-md-5">
                <div class="input-icon right">
                    <i class="fa fa-envelope"></i>
                    <input name="email" type="text" class="form-control" placeholder="Email Address" value="{{ old('email') ? old('email') : $user->email }}"> </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">CPF
            <span class="required"> * </span></label>
            <div class="col-md-5">
                <div class="input-icon right">
                    <i class="fa fa-user"></i>
                    <input name="cpf" type="text" class="form-control cpf-mask" placeholder="Enter CPF" value="{{ old('cpf') ? old('cpf') : $user->cpf }}"> </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">Data Nascimento</label>
            <div class="col-md-4">
                <div class="input-group input-icon col-md-4">
                <i class="fa fa-birthday-cake"></i>
                    <input name="birth_date" type="text" size="16" class="form-control form-control-inline input-medium date-mask" value="{{ old('birth_date') ? old('birth_date') : $user->birth_date != null ? $user->birth_date->format('d/m/Y') : "" }}">
                    <span class="input-group-btn">
                        <button class="btn default date-set" type="button">
                            <i class="fa fa-calendar"></i>
                        </button>
                    </span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">Data Admissão</label>
            <div class="col-md-4">
                <div class="input-group input-icon col-md-4">
                <i class="fa fa-thumb-tack"></i>
                    <input name="adm_date" type="text" size="16" class="form-control form-control-inline input-medium date-mask" value="{{ old('adm_date') ? old('adm_date') : $user->adm_date != null ? $user->adm_date->format('d/m/Y') : "" }}">
                    <span class="input-group-btn">
                        <button class="btn default date-set" type="button">
                            <i class="fa fa-calendar"></i>
                        </button>
                    </span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="single-prepend-text" class="control-label col-md-3">Departamento
            <span class="required"> * </span></label>
            <div  class="col-md-5">
            <div class="input-group select2-bootstrap-prepend">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button" data-select2-open="single-prepend-text">
                        <span class="fa fa-home"></span>
                    </button>
                </span>
                <input type="hidden" name="department_id">
                <select id="departments" name="department_id" class="form-control select2 required">
                    <option value=""></option>
                    @foreach($departments as $d)
                        <option {{$user->department_id === $d->id ? "selected" : ""}} value="{{ $d->id }}">{{ $d->description }}</option>
                    @endforeach    
                </select>
            </div>
            </div>
        </div>
        <div class="form-group">
            <label for="single-prepend-text" class="control-label col-md-3">Cargo
            <span class="required"> * </span></label>
            <div  class="col-md-5">
            <div class="input-group select2-bootstrap-prepend">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button" data-select2-open="single-prepend-text">
                        <span class="fa fa-black-tie"></span>
                    </button>
                </span>
                <input type="hidden" name="roles">
                <select id="roles" class="form-control select2 required">
                    <option value=""></option>
                    @foreach($roles as $r)
                        <option value="{{ $r->id }}">{{ $r->name }}</option>
                    @endforeach    
                </select>
            </div>
                <span class="help-inline">O cargo define as permissões do usuário dentro do sistema.</span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Senha
            <span class="required"> * </span></label>
            <div class="col-md-5" data-toggle="popover" data-placement="top" data-content="Deve ter no mínimo 6 caracteres.">
                <div class="input-icon right">
                    <i class="fa fa-lock"></i>
                    <input name="password" id="password" type="password" class="form-control" placeholder="Password"> 
                    <span class="help-inline"> Deve ter no mínimo 6 caracteres. </span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Confirmar Senha
            <span class="required"> * </span></label>
            <div class="col-md-5" data-toggle="popover" data-placement="top" data-content="Deve ter no mínimo 6 caracteres.">
                <div class="input-icon right">
                    <i class="fa fa-lock"></i>
                    <input name="password_confirm" name="password_confirm" type="password" class="form-control" placeholder="Password">
                </div>
            </div>
        </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-9">
            <button type="submit" class="btn btn-success">Salvar</button>
            <a class="btn default" href="{{ route('users.index') }}">Cancelar</a>
        </div>
    </div>
</div>