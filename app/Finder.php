<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Finder extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'telefone',
        'animal_id'
    ];

    /**
     * Get the animal that owns the finder.
     */
    public function animal()
    {
        return $this->belongsToMany('App\Animal');
    }
}
