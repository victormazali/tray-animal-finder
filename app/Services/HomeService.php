<?php

namespace App\Services;

use App\Facades\DateHelper;
use Illuminate\Http\Request;
use App\Repositories\AnimalRepository;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class HomeService
{
    /**
     * Repositório de animais
     * @var \App\Repositories\animalRepository
     */
    protected $animalRepository;

    /**
     * Método construtor
     *
     * @param \App\Repositories\AnimalRepository  $animalRepository  O repositório de animais
     */
    public function __construct(
        AnimalRepository $animalRepository
    ) {
        $this->animalRepository = $animalRepository;
    }

    /**
     * Finds animals.
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function findaAllAnimals()
    {
        return $this->animalRepository->findAnimalsForHome();
    }

}