<?php

namespace App\Services;

use App\Repositories\UserRepository;
use Illuminate\Support\Collection;
use Carbon\Carbon;

abstract class BaseService
{
	/**
     * Repositório de Usuários
     * @var \App\Repositories\UserRepository
     */
    protected $userRepository;

    public function __construct(
        UserRepository $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    /**
     * Método busca todos usuários através de seu repositório
     *
     * @return \Illuminate\Support\Collection
     */
    public function callAllUsers() {
        return $this->userRepository->findAll();
    }
}
