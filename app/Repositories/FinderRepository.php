<?php

namespace App\Repositories;

use App\Finder;
use App\Repositories\BaseRepository;
use Carbon\Carbon;

class FinderRepository extends BaseRepository
{
    /**
     * Método construtor
     *
     * @param \App\Finder  $model  O modelo
     */
    public function __construct(Finder $model)
    {
        parent::__construct($model);
    }
}
