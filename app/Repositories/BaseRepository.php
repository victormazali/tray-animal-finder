<?php

namespace App\Repositories;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

abstract class BaseRepository
{
    /**
     * Modelo
     * @var Model
     */
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * Método retorna todos registros d obanco
     *
     * @return \Illuminate\Support\Collection
     */
    public function findAll()
    {
        return $this->model->all();
    }

    /**
     * Encontra model pelo id
     *
     * @param  int     $id  O identificador
     *
     * @return <type>  ( description_of_the_return_value )
     */
    public function findOrFail(int $id)
    {
        return $this->model->findOrFail($id);
    }

    public function findWith(array $with, int $id)
    {
        return $this->model->with($with)->find($id);
    }

    /**
     * Método encontra registro do banco de dados atraves de uma condição
     *
     * @param  int     $value      O valor que
     * @param  string  $condition  A condição
     *
     * @return <type>  ( description_of_the_return_value )
     */
    public function findByCondition(int $value, string $condition = 'id', string $query = 'where')
    {
        return $this->model->$query($condition, $value)->first();
    }

    /**
     * { function_description }
     *
     * @param      integer  $value  The value
     *
     * @return     <type>   ( description_of_the_return_value )
     */
    public function whereCondition(int $value, string $condition = 'id', Builder $query = null)
    {
        if ($query === null) {
            $query = $this->model->newQuery();
        }
        
        return $this->model->where($condition, $value);
    }

    /**
     * Método encontra registro do banco de dados atraves de uma condição
     *
     * @param  int     $value      O valor que
     * @param  string  $condition  A condição
     * @param  string  $query      A pergunta
     *
     * @return <type>  A condição por.
     */
    public function getByCondition(int $value, string $condition = 'id', string $query = 'where')
    {
        return $this->model->$query($condition, $value);
    }

    /**
     * { function_description }
     *
     * @param      array   $arrayData  The array data
     * @param      string  $condition  The condition
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function whereIn(array $arrayData, string $condition = 'id')
    {
        return $this->model->whereIn($condition, $arrayData);
    }

    /**
     * Método realiza update do recurso no banco de dados.
     *
     * @param  array $data
     * @param  int $value
     * @param  string $condition
     * @return int $id | model
     */
    public function update(array $data, int $value, string $condition = 'id')
    {
        $model = $this->model->where($condition, $value)->first();

        if (is_null($model)) {
            return false;
        }

        $model->fill($data);
        $model->save($data);

        return $model;
    }

    /**
     * Método realiza update do recurso no banco de dados.
     *
     * @param  array $data
     * @param  int $value
     * @param  string $condition
     * @return int $id | model
     */
    public function updateWithTrashed(array $data, int $value, string $condition = 'id')
    {
        $model = $this->model->withTrashed()->where($condition, $value)->first();

        if (is_null($model)) {
            return false;
        }

        $model->fill($data);
        $model->save($data);

        return $model;
    }

    /**
     * Método armazena novo modelo no banco de dados.
     *
     * @param  array   $data  Os dados
     *
     * @return <type>  ( description_of_the_return_value )
     */
    public function store(array $data)
    {
        $model = $this->model->fill($data);
        $model->save($data);

        return $model;
    }

    /**
     * Método deleta o modelo do banco de dados
     *
     * @param      integer  $value      The value
     * @param      string   $condition  The condition
     *
     * @return     boolean  ( description_of_the_return_value )
     */
    public function delete(int $value, string $condition = 'id')
    {
        $model = $this->model->where($condition, $value)->first();

        if (is_null($model)) {
            return false;
        }

        return $model->delete();
    }

    /**
     * { function_description }
     *
     * @param      integer  $value      The value
     * @param      string   $condition  The condition
     *
     * @return     boolean  ( description_of_the_return_value )
     */
    public function restore(int $value, string $condition = 'id')
    {
        $model = $this->model->withTrashed()->where($condition, $value)->first();

        if (is_null($model)) {
            return false;
        }

        $model = $model->restore();

        return $model;
    }

    /**
     * Método deleta varios modelos do banco de dados
     *
     * @param      array   $ids    The identifiers
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function destroyMany(array $ids)
    {
        return $this->model->destroy($ids);
    }

    /**
     * Método armazena novos modelos no banco de dados.
     *
     * @param  array   $data  Os dados
     *
     * @return <type>  ( description_of_the_return_value )
     */
    public function insertGetId(array $data)
    {
        return $this->model->insertGetId($data);
    }

    /**
     * Gets the throw relation.
     *
     * @param      string  $relation   The relation
     * @param      string  $condition  The condition
     * @param      string  $value      The value
     *
     * @return     <type>  The throw relation.
     */
    public function getThrowRelation(string $relation, string $condition, string $value)
    {
        return $this->model->whereHas($relation, function ($query) use($condition, $value){
            $query->where($condition, $value);
        });
    }

    /**
     * Delete o modelo
     *
     * @param  int     $id  O identificador
     *
     * @return <type>  ( description_of_the_return_value )
     */
    public function destroy(int $id)
    {
        $model = $this->findOrFail($id);
        return $model->delete();
    }
}
