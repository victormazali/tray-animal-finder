<?php

namespace App\Http\Controllers;

use App\Department;
use App\Permission;
use App\Role;
use App\Schooling;
use App\Services\UserService;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Servico de Usuários
     * @var \App\Services\UserService
     */
    protected $userService;

    /**
     * Método construtor
     *
     * @param \App\Services\UserService  $userService  O servico de usuários
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }
    
    public function AuthRouteAPI(Request $request){
        return $request->user();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = $this->userService->findAllUsers();

        return view('user.index', compact('result'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = $this->userService->findAllDepartments();
        $roles = $this->userService->findAllRoles();
        $user = new User();

        return view('user.new')->with(compact('departments', 'roles', 'user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request = $this->userService->fillRequestUser($request);

        $user = $this->userService->storeUser($request);

        // Create the user
        if ( $user ) {

            flash('Usuário criado com sucesso.');

        } else {
            flash()->error('Não foi possível criar o usuário. Contate o administrador.');
        }

        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $departments = $this->userService->findAllDepartments();
        $roles = $this->userService->findAllRoles();

        return view('user.edit', compact('user', 'roles', 'departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request = $this->userService->fillRequestUser($request);

        $user = $this->userService->updateUser($request, $id);

        if ($user) {
            if ($request->has('roles')) {
                $this->syncPermissions($request, $user);
            }

            flash('Usuário alterado com sucesso.');

        } else {
            flash()->error('Não foi possível alterar o usuário. Contate o administrador.');
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Sync roles and permissions
     *
     * @param Request $request
     * @param $user
     * @return string
     */
    private function syncPermissions(Request $request, $user)
    {
        // Get the submitted roles
        $roles = $request->get('roles', []);
        $permissions = $request->get('permissions', []);

        // Get the roles
        $roles = Role::find($roles);

        // check for current role changes
        if( ! $user->hasAllRoles( $roles ) ) {
            // reset all direct permissions for user
            $user->permissions()->sync([]);
        } else {
            // handle permissions
            $user->syncPermissions($permissions);
        }

        $user->syncRoles($roles);

        return $user;
    }

    /**
     * { function_description }
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function register()
    {
        return view('auth.register');
    }

    /**
     * Stores a register.
     *
     * @param      \Illuminate\Http\Request  $request  The request
     *
     * @return     <type>                    ( description_of_the_return_value )
     */
    public function storeRegister(Request $request)
    {
        $request = $this->userService->fillRequestUser($request);

        $user = $this->userService->storeUser($request);

        // Create the user
        if ( $user ) {

            flash('Usuário criado com sucesso.');
            

            return redirect(url('/login'));

        } else {
            flash()->error('Não foi possível criar o usuário. Contate o administrador.');
        }

        return redirect()->route('users.index');
    }
}
