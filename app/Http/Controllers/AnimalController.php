<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\AnimalService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AnimalController extends Controller
{
    /**
     * Servico de Animais
     * @var \App\Services\AnimalService
     */
    protected $animalService;

    /**
     * Método construtor
     *
     * @param \App\Services\AnimalService  $animalService  O servico de animais
     */
    public function __construct(AnimalService $animalService)
    {
        $this->animalService = $animalService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = $this->animalService->findAnimalsByUser();

        return view('animals.index')->with(compact('result'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $animal = new \App\Animal();

        return view('animals.new')->with(compact('animal'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request = $this->animalService->fillRequestAnimal($request);

        $animal = $this->animalService->storeAnimal($request);

        // Create the animal
        if ( $animal ) {

            flash('Animal cadastrado com sucesso.');

        } else {
            flash()->error('Não foi possível cadastrar o animal. Contate o administrador.');
        }

        return redirect()->route('animals.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        $animal = $this->animalService->findOrFailAnimal($id);

        return view('animals.edit', compact('animal'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        $request = $this->animalService->fillRequestAnimal($request);
        
        if ($this->animalService->updateAnimal($request->all(), $id)) {
            flash()->success('Animal editado com sucesso.');
        } else {
            flash()->error('Não foi possível editar o Animal.');
        }

        return redirect()->route('animals.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        if ($this->animalService->destroyAnimal($id)) {
            flash()->success('Animal removido com sucesso');
        } else {
            flash()->success('Não foi possível remover o animal.');
        }

        return redirect()->route('animals.index');
    }

    /**
     * Método registra o comunicado de um animal
     *
     * @param      \Illuminate\Http\Request  $request  The request
     */
    public function foundTheAnimal(Request $request)
    {
        $finder = $this->animalService->storeFinder($request);

        $animal = $this->animalService->updateAnimal(['status_id' => 2], $request->input('animal_id'));

        if ($finder && $animal) {
            flash()->success('Comunicado enviado com sucesso');
        } else {
            flash()->success('Não foi possível efetuar o comunicado.');
        }

        return redirect()->route('home');
    }

    /**
     * Shows the finders.
     *
     * @param      integer  $id     The identifier
     *
     * @return     <type>   ( description_of_the_return_value )
     */
    public function showFinders(int $id)
    {
        $animal = $this->animalService->findOrFailAnimal($id);

        return view('animals.finders', compact('animal'));
    }
}
