<?php

namespace App\Http\Controllers;

use App\Services\HomeService;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Servico de Animais
     * @var \App\Services\AnimalService
     */
    protected $homeService;

    /**
     * Método construtor
     *
     * @param \App\Services\HomeService  $homeService  O servico de animais
     */
    public function __construct(HomeService $homeService)
    {
        $this->homeService = $homeService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = $this->homeService->findaAllAnimals();

        return view('pages.home')->with(compact('result'));
    }
}
