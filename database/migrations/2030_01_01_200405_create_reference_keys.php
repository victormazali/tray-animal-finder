<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferenceKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('animals', function($table)  {
            $table->foreign('status_id')->references('id')->on('status_animals');
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('finders', function($table)  {
            $table->foreign('animal_id')->references('id')->on('animals')->onDelete('cascade');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('animals', function($table)  {
            $table->dropForeign('animals_status_id_foreign');
            $table->dropForeign('animals_user_id_foreign');
        });

        Schema::table('finders', function($table)  {
            $table->dropForeign('finders_animal_id_foreign');
        });
    }
}
