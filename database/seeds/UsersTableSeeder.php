<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Victor Mazali',
            'email' => 'victor.mazali@tray.com.br',
            'password' => bcrypt('123321')
        ]);
    }
}
